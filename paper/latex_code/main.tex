\documentclass[letterpaper, 10 pt, conference]{IEEEtran} 
%\documentclass{article}
\usepackage[final]{pdfpages}
\usepackage{authblk}
\usepackage{titlesec}
\usepackage{amsmath}

\titlespacing*{\section}
{0pt}{1ex}{1ex}
\titlespacing*{\subsection}
{0pt}{0ex}{0ex}
\titlespacing*{\subsubsection}
{0pt}{0ex}{0ex}

\usepackage{etoolbox}

\BeforeBeginEnvironment{figure}{\vskip-2.8ex}
\AfterEndEnvironment{figure}{\vskip-1ex}
\BeforeBeginEnvironment{table}{\vskip-2.8ex}
\AfterEndEnvironment{table}{\vskip-3ex}

\usepackage{caption}
\captionsetup{skip=1pt}


\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{enumerate}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}
\usepackage{float}
\usepackage{wrapfig}
\usepackage{subcaption}
\usepackage{caption}
\usepackage{siunitx}
\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}
    
\begin{document}

\title{Swarm Intelligence Based Maze Solver}

\author[1*]{Digya Acharya}
\author[1]{Pranish Bhagat}
\author[1]{Pratik Bhandari}
\author[1]{Sijan Bhattarai}
\author[2]{Dinesh Baniya Kshatri}
\affil[1,2]{Department of Electronics and Computer Engineering}
\affil[1]{Institute of Engineering, Pulchowk Campus, Tribhuvan University}
\affil[2]{Institute of Engineering, Thapathali Campus, Tribhuvan University}
\affil[1*]{\textit{digyaacharya753@gmail.com}}

\maketitle

% Keywords command
\providecommand{\keywords}[1]
{
  \small	
  \textbf{\textit{Index Terms---}} #1
}

\begin{abstract}
Efficient collision-free motion through environments comprising of obstacles is crucial to the development of autonomous navigation systems used for target detection and tracking. In this paper, we have designed a two-robot system with swarm intelligence whereby one bot assists the other to autonomously navigate through a maze and arrive at the destination. The bot commencing the navigation uses data from infrared sensors to avoid obstacles whilst following the \textit{Left Wall Follower Algorithm}. Upon reaching the target position, the initial bot wirelessly transfers the unoptimized path information recorded while traversing the maze to the second bot which then extracts the optimum path and autonomously follows it. We have used short-range Bluetooth for communication and Arduino Uno chip-set to program both the bots. Rather than stepper motors, we have opted for direct current motors together with differential kinematics. This hardware modification avoids slippage, reduces cost, increases the degrees of freedom and speed of the bots. Furthermore, the establishment of a communication pathway between two Arduinos using a Bluetooth module for coordinated search with a novel encoding algorithm is a significant milestone of this project.
\end{abstract}

\keywords{\textbf{\emph{Bluetooth, Left Wall Follower Algorithm, Maze Solving, Swarm Intelligence}}}

\section{Introduction}
\textit{Sahin et.al}\cite{sahin} defined swarm robotics as a novel approach to the coordination of large numbers of robots and the study of how large numbers of relatively simple physically embodied agents can be designed such that a desired collective behavior emerges from the local interactions among agents and also between the agents and the environment. \textit{Beni}\cite{beni}, scholar, University of California describes this kind of robots’ coordination as: “The group of robots is not just a group. It has some special characteristics, which are found in swarms of insects i.e. decentralized control, lack of synchronization, simple and (quasi) identical members.” Although swarm robotics is still in its infancy, it can effectively be adopted to tackle many real-world engineering problems. Some of them are natural disaster zones\cite{Arnold2018, 7882983}, hostage rescue situations\cite{zhang, winfield}, navigation in unknown territory\cite{ducatelle, banks_vincent_phalp_2008, TAN201318} and waste removal\cite{hsieh}. The communication between two bots with different complexities can even enable the bot with lower complexity grade achieve more intelligence than the one with higher complexity. The second bot and all the other subsequent bots require less hardware and computational power. 

For the robot to traverse an unknown surrounding and identify obstacles successfully, certain sensors are required. Infrared (IR) sensors\cite{Ismail_2016, rahman} and ultrasonic sensors\cite{win} have widely been used in such robots. Among them, IR sensors are more extensively used because of their narrow range of field whereas ultrasonic sensors have a wider sensing area\cite{sasidharan, mustapha}. Light Detection and Ranging (LiDAR) is another technology that has brought recent developments in the use of autonomous navigation\cite{doi:10.5772/56125}.

In this paper, Bluetooth technology has been used to establish a communication link between two robots for its simplicity and ease of use within a short-range. Similarly, the use of differential kinematics in direct current (DC) motors enabled smooth movement and direction control by providing more degrees of freedom. Since the trajectory used in this project is a wall-linked maze with the target point located at the periphery, Left Wall Follower Algorithm is implemented.

\section{Motivation}
Unlike a single bot, which emulates the action of humans and accomplishes the tasks solely\cite{Coradeschi, Wu2009HumanInspiredRT}, the swarm robotics is inspired by the behaviour of social insects. Inside the colony of these insects, there are interactions between the individuals and between the individuals and the environment\cite{gordon, fewell}. These interactions propagate throughout the colony and enable them to solve the tasks that couldn't have been accomplished by a lone bot. By adopting the characteristics of social insects in a multi-robot setting, the robotic swarm can be made robust to individual failure with fewer hardware components and adaptable to solve unforeseen maze complexities in the shortest time. 

\section{Methodology}
\subsection{System Architecture}
The system contains two robotic agents. First, one of the agents traverses the whole maze and relays the information it gathered to the second agent via Bluetooth as demonstrated in Fig.~\ref{fig:1}.

\begin{figure}[H]
\centering
\centerline{\includegraphics[height=210pt, width=170pt]{imageedit_40_5345772697.png}}
\centering
\caption{Overall System Architecture}
\label{fig:1}
\end{figure}

The maze solving is carried out in several steps as can be seen from the functional block diagram in Fig.~\ref{fig:2}. The IR sensors of the first robot detect the walls based on which the movement decision is made by the ATmega328P microcontroller. As the motor action drives the robot either left, straight, right or back, it reaches a junction at some point. This process continues until the target is detected by the smoke sensor (MQ-2 Gas Sensor).
\vspace{0.2em}
\begin{figure}[H]
\centering
\centerline{\includegraphics[height=152pt, width=227pt]{functional_finalized.png}}
\centering
\caption{Functional Block Diagram of the System}
\label{fig:2}
\end{figure}
\vspace{-0.5em}
While traversing through the maze, the first bot encodes and stores the direction changes that it makes. After the target gets detected, it calculates the shortest path information which is then encoded and sent to the second bot via Bluetooth. At the receiving side, the second robot decodes the message it obtains from the first bot to extract the optimum path. The second bot uses the optimum path information to guide itself to the destination. It is also equipped with IR sensors to restrict the deviation from the scheduled path.
\subsection{Maze Structure}

\begin{figure}[H]
\centering
\centerline{\includegraphics[width=263pt]{3d-view_updated.jpg}}
\caption{Three-dimensional view of the Maze}
\label{fig:3}
\end{figure}
\vspace{-0.5em}
The walls of the maze have a uniform height of 6 inches. The width of the path that the robot drives in is 14.2 inches. The appropriate distance between the two walls for the robot to drive through was selected by a trial and error process where the robot was made to run through the paths of various widths. Suitable width was required for the robot to make U-turn properly without making contact with the walls. Similarly, the sensing distance for the right and left sensors was kept in mind while determining the width.

For the purpose of implementing \textit{Left Wall Follower algorithm}, a single-entry, single-exit maze was designed. The maze walls were connected with cellulose-based adhesive tape. The start and target point of the maze was selected as shown in Fig. 3.

\subsection{Robot Configuration}

Both the agents used in this project have two rear wheels rotated using a DC motor mounted on the axle of the wheel. Each of the rear wheels has two degrees of freedom i.e. they can be rotated in two directions (forward and backward).
At the front end of each of the robots is a caster ball wheel which helps in balancing it. The use of the caster ball wheel can be justified as our project is not concerned with the motion in the rough terrain.
The physical dimensions of the robots is illustrated in Table I.
\vspace{0.5em}
\begin{table}[H]
\caption{Physical Dimension of Robots}
\begin{tabular}{ |p{3cm}|p{4.15cm}| } 
\hline
\textbf{Attributes} & \textbf{Dimension (in inch)}\\
\hline
Length & 8.27\\
\hline
\row Width & 3.93 (short side) / 5.9 (longer side)\\ 
\hline
\row Diameter of rear wheels & 2.52\\
\hline
\row Diameter of caster wheel & 0.433\\
\hline
\end{tabular}
\label{dimension}
\end{table}
\vspace{0.5em}

Aside from the descriptions above, each of the robots consists of several sensor modules. The first robot has 3 IR sensors, each attached to the front, left and right side of the body as shown in Fig.~\ref{fig:4}a. The smoke sensor is attached to the front end beside the IR sensor. Similarly, the Bluetooth module HC-05 is placed at the rear end. With the motor controller at the center of the body and the Arduino behind it, the battery had to be connected at the back side. Small holes are drilled at various locations of the body to allow sensors and modules to be attached firmly. For utilizing the benefits of swarm intelligence, the second robot is intentionally made simpler and does not contain the smoke sensor (Fig.~\ref{fig:4}b). 

\begin{figure}[H]
\begin{subfigure}{\linewidth}
\centering{\includegraphics[width=200, height=110]{first_robot_final.png}}
\caption{{First Robot}}
\end{subfigure}\vfill
\vspace{0.5em}
\begin{subfigure}{\linewidth}
\centering{\includegraphics[width=200, height=110]{second_robot_final.png}}
\caption{{Second Robot}}
\end{subfigure}
\caption{Schematic Representation of Robots}
\label{fig:4}
\end{figure}

\vspace{-0.4em}
The H-Bridge motor controller (L298N) is used as an interface between the microcontroller and DC motor to prevent the circuit from damage and automate the switch of the polarity of voltage applied to motors. The supply to the UNO is provided via the 5V output socket of the motor controller which is powered by the Lithium Polymer battery. The final view of both the robots after all the configurations is displayed in Fig.~\ref{fig:5}. 

\begin{figure}[H]
\begin{minipage}{0.45\linewidth}
\centering
\centerline{\includegraphics[width=130, height=80]{first_bot-removebg-preview-removebg-preview.png}}
\subcaption{First Robot}
\end{minipage}
\hfill
\begin{minipage}{0.45\linewidth}
\centering
\centerline{\includegraphics[width=130, height=80]{second_bot_traversing_maze-removebg-preview.png}}
\subcaption{Second Robot}
\end{minipage}
\caption{Final view of the Robots}
\label{fig:5}
\end{figure}

\subsection{Robot Locomotion}
\begin{figure}[H]
\centering
\centerline{\includegraphics[width=200, height=160]{differential_sharpen.png}}
\caption{Arrangement of Differential Drive Kinematics}
\label{fig:6}
\end{figure}

Each of the robots move with a differential drive and contains 6 degrees of freedom expressed by the pose: $(x, y, z, Roll, Pitch, Yaw)$. Here, $(x, y, z)$ is the position and $(Roll, Pitch, Yaw)$ provides the altitude information. If $v_{r}$ and $v_{l}$ are the velocities of the right and left wheels, $T$ is the time taken by wheels to complete one full turn around Instantaneous Center of Curvature (ICC), $R$ is the distance between ICC and the midpoint of the wheel axis,  $l$ is the length of the wheel axis and $w$ is the angular velocity then, the velocity of each of the wheels is given by:

\begin{equation} \label{eq:line1}
v_{r} = \omega(R+l/2)
\end{equation}

\begin{equation} \label{eq:line2}
v_{l} = \omega(R - l/2)
\end{equation}

By varying the velocity of the wheels, we can alter the trajectory and angular velocity in which the robot moves:

\begin{equation} \label{eq:line3}
R = \frac{l}{2}\frac{(v_{l}+v_{r})}{(v_{l} - v_{r})}
\end{equation}

\begin{equation} \label{eq:line4}
w = \frac{(v_{r} - v_{l})}{l}
\end{equation}

There is a forward linear motion in a straight line if $v_{l} = v_{r}$, the wheels rotate in place about the midpoint of the wheel axis if $v_{l} = -v_{r}$ and there is a rotation about the left and right wheel if $v_{l} = 0$, $v_{r} = 0$ and $R = l/2$. Using differential kinematics, robot can easily move and change its direction because of more degrees of freedom. The rear wheels are driven by the Pulse Width Modulation (PWM) signal generated from the processor, which in our case is ATmega328P. PWM signal affects the differential drive and motion of the wheels.

\vspace{0.5em}
\subsection{Maze Traversal}

Left wall follower algorithm has been used for maze traversal which moves following the sequence in Fig.~\ref{fig:7}.

\begin{figure}[H]
\centering
\centerline{\includegraphics[width=200, height=80]{m_order.png}}
\caption{Movement Precedence in Left Wall Follower Algorithm}
\label{fig:7}
\end{figure}

The first agent, irrespective of the position of the target, always prefers the left path over a
straight path and straight path over the right one. The second robot, however, has to eliminate the mistakes made by the first robot to avoid reaching the dead ends. 
The information about the structure of the maze and decisions made at every junction gathered by the first agent needs to be molded into the appropriate payload that can be sent through the Bluetooth module HC-05 to the second robot. 


\vspace{0.5em}
\begin{table}[H]
\caption{Encoded Binary Values of the Decisions made by the Robot}
\centering
\begin{tabular}{ |p{2cm}|p{1cm}| } 
\hline
\textbf{Decision} & \textbf{Bits}\\
\hline
\row Left & 001\\ 
\hline
\row Straight & 010\\
\hline
\row Right & 011\\
\hline
\row U-turn & 101\\
\hline
\end{tabular}
\label{encoded_bits}
\end{table}

\vspace{0.22em}
The payload for the Bluetooth communication can be generated by encoding the distance covered by the robot between the points of interest, but for the implementation of this, we need an encoder. However, our implementation algorithm encodes the decision made at every junction and does not require an integrated circuit of the encoder. 

In this implementation model, all the junctions are assigned a number which gives the position of the junction. Then, the decisions made at each junction are sent as a string of encoded binary data which are later decoded by the second robot in course of finding the
most optimal path. All the possible decisions that can be made at any junction are encoded using 3-bits as
shown in table \ref{encoded_bits}. 3-bits have been used to encode four possible decisions because, during the decoding step, the second robot substitutes the undesired decisions with bits “000" to eliminate those decisions. Thus, the use of 2-bit encoding would have caused a problem during the elimination of the undesired message bits.


\section{Results}
From the initial position, the first robot traversed the maze and the required decisions
at the junctions were taken based on the priorities provided by the left wall follower
algorithm. The final path travelled by the first robot is displayed in Fig.~\ref{fig:8}. The robot detected the target (smoke) at the finish position using MQ-2 Gas Sensor as shown in Fig.~\ref{fig:10}. For the detection of the target, the sensor analog output voltage was read and when it reached
a certain threshold, the target detection decision was taken as positive and subsequently, the
first robot was stopped for any locomotion thereafter.

\begin{figure}[H]
\centering
\centerline{\includegraphics[width=200, height=110]{fig8a_final.png}}
\caption{Path Travelled by First Robot}
\label{fig:8}
\end{figure}

\begin{figure}[H]
\centering
\centerline{\includegraphics[width=200, height=110]{fig9_sharpen.png}}
\caption{Path Travelled by Second Robot}
\label{fig:9}
\end{figure}

Both the LEDs of the master and slave setup HC-05 blinked at the rate of two fast blinks
every two seconds indicating that they have paired with each other. The encoded decisions taken at the junctions were then decoded and were sent to the second robot via Bluetooth.

\begin{figure}[H]
\centering
\centerline{\includegraphics[width=160, height=100]{smoke.png}}
\caption{Detection of Target at the Finish Position}
\label{fig:10}
\end{figure}

Thus, based upon the decoded message, the second robot traversed the maze in the shortest path possible as demonstrated in Fig.~\ref{fig:9}. 
The decision taken at various junctions by the first robot are:

\vspace{0.5em}
\begin{table}[H]
\begin{center}
\begin{tabular}{ |p{0.2cm}|p{0.2cm}|p{0.2cm}|p{0.2cm}|p{0.2cm}|p{0.2cm}|p{0.2cm}|p{0.2cm}|p{0.2cm}|p{0.2cm}|p{0.2cm}|p{0.2cm}|p{0.2cm}|} 
\hline
S & L & U & S & L & U & S & L & U & L & S & U & L\\
\hline
\end{tabular}
\label{decision}
\end{center}
\end{table}
 Here, `S' denotes the straight path, `R' denotes right direction, `L' signifies left and `U' means U-Turn. The decoded decisions sent to the second robot are:
\vspace{0.7em}
\begin{table}[H]
\begin{center}
\begin{tabular}{ |p{0.6cm}|p{0.6cm}|p{0.6cm}|p{0.6cm}|p{0.6cm}|}
\hline
S & R & R & S & R \\
\hline
\end{tabular}
\label{decoded}
\end{center}
\end{table}

We also examined the PWM waveforms of DC motors (Fig.~\ref{fig:11}). For this, the direction of current flow in H-Bridge was changed and the robot was moved in left, right, clockwise and anticlockwise direction. From the PWM waveforms, we observed that motor 2 is mobilized 75 percent of the time during motion in the left direction and motor 1 is used the most during rightward motion. During both left and right motion, the other motor remains stationary while one is moving. Similarly, for the motion in the clockwise direction, both the motors are operated most of the time and they are least exploited in the anticlockwise direction. 

\begin{figure}[H]
\begin{minipage}{0.45\linewidth}
\includegraphics[width=100, height=70]{left_final_sharpen.png}
\centering
\subcaption{}
\centering
\end{minipage}
\hfill
\begin{minipage}{0.45\linewidth}
\centering
\includegraphics[width=100, height=70]{right_sharpen.png}
\centering
\subcaption{}
\end{minipage}%

\begin{minipage}{0.45\linewidth}
\centering
\includegraphics[width=100, height=70]{clockwise_sharpen.png}
\centering
\subcaption{}
\end{minipage}
\hfill
\begin{minipage}{0.45\linewidth}
\centering
\includegraphics[width=100, height=70]{anti_clockwise_sharpen.png}
\centering
\subcaption{}
\end{minipage}%
\caption{PWM Waveforms of DC Motors (a) when motor was moved in the left direction (b) when motor was moved in the right direction (c) when motor was moved in the clockwise direction (d) when motor was moved in the anticlockwise direction}
\label{fig:11}
\end{figure}

\section{Conclusion and Future Enhancements}

In this paper, we demonstrated the concept of swarm robotics in maze solving using Left Wall Follower Algorithm. We also used a new method to encode the bits while communicating through Bluetooth. By using castor wheels, H-bridge and DC motors with differential kinematics for locomotion and interface, we avoided unforeseen problems like slippage, circuit damage and rotation issues. 

In the future, we plan to use robust IR sensors that work under a variety of lighting conditions, integrate the camera with the robots to map the maze surrounding and implement dynamic path planning. We will also use the IEEE 802.11 protocol for communication and add more robots in the swarm.

\medskip
 
\bibliographystyle{unsrt}
\bibliography{newreferences}
\end{document}
